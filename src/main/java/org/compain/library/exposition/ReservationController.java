package org.compain.library.exposition;


import org.compain.library.security.ClientToken;
import org.compain.library.service.DTO.InfoReservation;
import org.compain.library.service.DTO.ReservationDTO;
import org.compain.library.service.DTO.ReservationNotification;
import org.compain.library.service.DTO.UserLateBorrowing;
import org.compain.library.service.MailService;
import org.compain.library.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/reservations")
public class ReservationController {


    private ReservationService reservationService;


    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping("/create")
    public void createReservation(@RequestBody ReservationDTO reservationDTO, ClientToken user){
        reservationService.save(reservationService.createReservation(reservationService.getBookReservationInfo(user.getUserId(), reservationDTO.getIdBook()), user.getUserId()));
    }

    @PostMapping("/delete")
    public void deleteReservation(@RequestBody Long idReservation, ClientToken user){
        reservationService.deleteReservation(idReservation);
    }

    @GetMapping("/user")
    public List<ReservationDTO> getUserReservation(ClientToken user){
     return reservationService.getUserReservation(user.getUserId());
    }

    @GetMapping("/book")
    public InfoReservation getBookInfoReservation(ClientToken user, @RequestParam Long idBook){
        return reservationService.getBookReservationInfo(user.getUserId(), idBook);
    }

    @GetMapping("/to-notify")
    public List<ReservationNotification> getReservationToNotify(ClientToken user) {
        return reservationService.findReservationToReplaceLateReservation();
    }

    @PostMapping("/notify-user")
    public void sendMailForNotifyUserReservationIsAvailable(@RequestBody ReservationNotification reservationNotification, ClientToken user) throws MessagingException {
        reservationService.sendMailForNotifyReservation(reservationNotification);
    }
}
