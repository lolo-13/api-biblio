package org.compain.library.consumer;

import org.compain.library.model.Book;
import org.compain.library.model.Reservation;
import org.compain.library.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {

    @Query(value = "SELECT * FROM reservation " +
            "WHERE id_user = :idUser",
            nativeQuery = true)
    List<Reservation> findReservationByUser(Long idUser);

    @Query(value = "SELECT * FROM reservation " +
            "WHERE id_book = :idBook " +
            "ORDER BY reservation_date ",
            nativeQuery = true)
    List<Reservation> findReservationByBook(Long idBook);

    @Query(value = "SELECT * FROM reservation " +
            "WHERE id_book = :idBook " +
            " AND notified = false",
            nativeQuery = true)
    List<Reservation> findNotNotifiedReservationByBook(Long idBook);

    Optional<Reservation> findById(Long idReservation);

    @Query(value = "SELECT * FROM reservation " +
            "WHERE notified = true ",
            nativeQuery = true)
    List<Reservation> findAllNotifiedReservation();

    @Query(value = "SELECT count(*) FROM reservation WHERE id_book= :idBook" ,
            nativeQuery = true
    )
    int numberOfReservationForBook(Long idBook);

    @Query(value = "SELECT * FROM reservation " +
            "WHERE id_user = :idUser " +
            "AND id_book = :idBook",
            nativeQuery = true)
    Optional<Book> bookReservedByUser(Long idUser, Long idBook);
    boolean existsByUserAndBook(User user, Book book);

    List<Reservation> findByBook(Book book);

    @Query( value =  "SELECT * FROM reservation b " +
            "INNER JOIN users c ON b.id_user = c.id_user " +
            "WHERE id_book = :idBook " +
            "AND c.id_library = :idLibrary " +
            "AND notified = false " +
            "ORDER BY reservation_date " +
            "LIMIT 1" ,
            nativeQuery = true)
    Reservation findFirstReservationForBook(Long idBook, Long idLibrary);

    @Query( value =  "SELECT * FROM reservation " +
            "WHERE notification_date > :date " +
            "AND notified = true ",
            nativeQuery = true)
    List<Reservation> findReservationNotifiedAndDelayedPassed(LocalDateTime date);

    @Query( value =  "SELECT * FROM reservation " +
            "WHERE notified = true ",
            nativeQuery = true)
    List<Reservation> findReservationNotified();
}
