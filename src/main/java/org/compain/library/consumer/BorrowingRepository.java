package org.compain.library.consumer;

import org.compain.library.model.Book;
import org.compain.library.model.Borrowing;
import org.compain.library.model.Copy;
import org.compain.library.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Repository
public interface BorrowingRepository extends JpaRepository<Borrowing, Long> {


    @Query(
            value =  "SELECT * FROM borrowings s " +
                    "WHERE s.id_user = :idUser",
            nativeQuery = true
    )
    List<Borrowing> findByIdUser(Long idUser);
    Borrowing findByIdBorrowing(Long idBorrowing);
    @Query(
            value =  "SELECT * FROM borrowings s " +
                    "WHERE s.borrowing_limit_date < :dateTime " +
                    "AND returned = false",
            nativeQuery = true
    )
    List<Borrowing> findLateBorrowing(LocalDateTime dateTime);

    @Query(
            value =  "SELECT * FROM borrowings b " +
                    "INNER JOIN copies c ON b.id_copy = c.id_copy " +
                    "WHERE b.id_user = :idUser " +
                    "AND c.id_book = :idBook",
            nativeQuery = true
    )
    Optional<Borrowing> findByBookAndUser(Long idUser, Long idBook);

    @Query(
            value =   "SELECT borrowing_limit_date FROM borrowings b " +
            "INNER JOIN copies c ON b.id_copy = c.id_copy " +
            "WHERE returned = 'false' " +
            "AND c.id_book = :idBook " +
            "ORDER BY borrowing_limit_date " +
            "LIMIT 1",
            nativeQuery = true
    )
    LocalDateTime findFirstReturn(Long idBook);
}
