package org.compain.library.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;


@Data
@Entity
@Table(name = "Reservation")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_reservation", unique = true)
    private Long idReservation;
    @OneToOne
    @JoinColumn( name="id_book", nullable=false )
    private Book book;
    @OneToOne
    @JoinColumn( name="id_user", nullable=false )
    private User user;
    @Column(name = "notification_date")
    private LocalDateTime notificationDate;
    @Column(name = "notified")
    private Boolean notified;
    @Column(name = "reservation_date", nullable=false )
    private LocalDateTime reservationDate;

    public Reservation setNotified(Boolean notified){
        this.notified = notified;
        return this;
    }

}
