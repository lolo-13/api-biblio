package org.compain.library.service;

import org.compain.library.consumer.BorrowingRepository;
import org.compain.library.consumer.CopyRepository;
import org.compain.library.consumer.UserRepository;
import org.compain.library.model.*;
import org.compain.library.service.DTO.BorrowingDTO;
import org.compain.library.service.DTO.InfoBorrowingDTO;
import org.compain.library.service.DTO.ReservationDTO;
import org.compain.library.service.DTO.UserLateBorrowing;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
public class BorrowingService {

    private final BorrowingRepository borrowingRepository;
    private final UserRepository userRepository;
    private final CopyRepository copyRepository;
    private final ReservationService reservationService;

    public BorrowingService(BorrowingRepository borrowingRepository, UserRepository userRepository, CopyRepository copyRepository, ReservationService reservationService) {
        this.borrowingRepository = borrowingRepository;
        this.userRepository = userRepository;
        this.copyRepository = copyRepository;
        this.reservationService = reservationService;
    }

    public List<BorrowingDTO> findAll() {
        List<Borrowing> borrowings = borrowingRepository.findAll();
        return borrowings.stream().map(BorrowingMapper::toDTO).collect(toList());
    }

    public List<InfoBorrowingDTO> findAllByIdUser(Long idUser) {
        List<Borrowing> borrowings = borrowingRepository.findByIdUser(idUser);
        return borrowings.stream().map(BorrowingMapper::infoBorrowingDTO).collect(toList());
    }

    public BorrowingDTO findByIdBorrowing(Long idBorrowing) {
        return BorrowingMapper.toDTO(borrowingRepository.findByIdBorrowing(idBorrowing));
    }

    public List<UserLateBorrowing> findLateBorrowing(LocalDateTime dateTime) {
        List<Borrowing> borrowings = borrowingRepository.findLateBorrowing(dateTime);
        Map<User, List<Borrowing>> collect = borrowings.stream().collect(Collectors.groupingBy(Borrowing::getUser));
        return collect.entrySet().stream().map(BorrowingMapper::toUserLateBorrowing).collect(Collectors.toList());
    }

    public void updateBorrowing(BorrowingDTO borrowingPatched) {
        Borrowing oldBorrowing = borrowingRepository.findByIdBorrowing(borrowingPatched.getIdBorrowing());
        borrowingRepository.save(BorrowingMapper.patch(borrowingPatched, oldBorrowing));
    }

    public Borrowing createNewBorrowing(BorrowingDTO newBorrowingDTO) {
        Optional<User> optionalUser = userRepository.findByUsername(newBorrowingDTO.getUserDto().getEmail());
        Optional<Copy> optionalCopy = copyRepository.findById(newBorrowingDTO.getCopyDto().getIdCopy());
        Borrowing borrowing = new Borrowing();
        if (optionalUser.isPresent() && optionalCopy.isPresent()) {
            Copy copy = optionalCopy.get();
            User user = optionalUser.get();
            if(copy.getAvailable() == true) {
                borrowing = BorrowingMapper.toEntity(newBorrowingDTO, user, copy);
                List<ReservationDTO> reservation = reservationService.getUserReservation(user.getIdUser());
                if (reservation != null) {
                    deleteReservationForNewBorrowing(reservation, copy);
                }
            }
        }
        return borrowing;
    }


    public ReservationDTO deleteReservationForNewBorrowing(List<ReservationDTO> userReservation, Copy copy) {
        ReservationDTO erasedReservation = null;
        for (ReservationDTO reservation : userReservation
        ) {
            if (reservation.getIdBook().equals(copy.getBook().getIdBook())) {
                reservationService.deleteReservation(reservation.getIdReservation());
                erasedReservation = reservation;
            }
        }
        return erasedReservation;
    }

    public void save(Borrowing borrowing){
        if(borrowing != null) {
            borrowingRepository.save(borrowing);
            Copy copy = borrowing.getCopy();
            copy.setAvailable(false);
            copyRepository.save(copy);
        }
    }

    //#FIX ticket 2 Prolongation du prêt impossible si date dépassée

    public Borrowing renewBorrowing(long idBorrowing) {
        LocalDateTime today = LocalDateTime.now();
        Borrowing borrowing = borrowingRepository.findByIdBorrowing(idBorrowing);
        if (!borrowing.getRenewal() && borrowing.getBorrowingLimitDate().isAfter(today)) {
            borrowing.setBorrowingLimitDate(borrowing.getBorrowingLimitDate().plusWeeks(4));
            borrowing.setRenewal(true);
            borrowingRepository.save(borrowing);
        }
        return borrowing;
    }

    public Borrowing returnBorrowing(Long idBorrowing, Long idUser) {
        Optional<User> user = userRepository.findByIdUser(idUser);
        Borrowing borrowing = borrowingRepository.findByIdBorrowing(idBorrowing);
        if (user.isPresent()) {
            Copy copy = borrowing.getCopy();
            copy.setAvailable(true);
            copyRepository.save(copy);
            Long idLibrary = user.get().getLibrary().getIdLibrary();
            borrowing.setReturned(true);
            borrowingRepository.save(borrowing);
            Book book = borrowing.getCopy().getBook();
            Reservation firstReservation = reservationService.getFirstReservationForBook(book.getIdBook(), idLibrary);
            if (firstReservation != null) {
                reservationService.sendMailForNotifyReservation(ReservationMapper.toReservationNotification(firstReservation));
            }
        }
        return borrowing;
    }
}
