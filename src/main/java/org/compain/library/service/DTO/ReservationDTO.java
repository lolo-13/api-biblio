package org.compain.library.service.DTO;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ReservationDTO {
    private Long idReservation;
    private Long idUser;
    private Long idBook;
    private String title;
    private LocalDateTime reservationDate;
    private Boolean notified;
    private LocalDateTime notificationDate;

}
