package org.compain.library.service.DTO;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Data
public class InfoReservation {
    private Long idBook;
    private Integer maxReservation;
    private Integer currentReservation;
    private LocalDateTime firstReturn;
    private Boolean reservable;
    private Integer myPosition;
}
