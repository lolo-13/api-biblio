package org.compain.library.service;

import org.compain.library.model.Book;
import org.compain.library.model.Reservation;
import org.compain.library.model.User;
import org.compain.library.service.DTO.ReservationDTO;
import org.compain.library.service.DTO.ReservationNotification;

public class ReservationMapper {

    public static ReservationDTO toDTO(Reservation reservation) {
        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setIdReservation(reservation.getIdReservation());
        reservationDTO.setIdUser(reservation.getUser().getIdUser());
        reservationDTO.setIdBook(reservation.getBook().getIdBook());
        reservationDTO.setTitle(reservation.getBook().getTitle());
        reservationDTO.setReservationDate(reservation.getReservationDate());
        reservationDTO.setNotified(reservation.getNotified());
        reservationDTO.setNotificationDate(reservation.getNotificationDate());
        return reservationDTO;
    }

    public static Reservation toEntity(ReservationDTO reservationDTO, User user, Book book){
        Reservation reservation = new Reservation();
        reservation.setUser(user);
        reservation.setBook(book);
        reservation.setReservationDate(reservationDTO.getReservationDate());
        reservation.setNotified(reservationDTO.getNotified());
        reservation.setNotificationDate(reservation.getNotificationDate());
       return reservation;
    }

    public static ReservationNotification toReservationNotification(Reservation reservation){
        ReservationNotification reservationNotification = new ReservationNotification();
        reservationNotification.setEmail(reservation.getUser().getEmail());
        reservationNotification.setFirstname(reservation.getUser().getFirstname());
        reservationNotification.setName(reservation.getUser().getName());
        reservationNotification.setBook(reservation.getBook().getTitle());
        reservationNotification.setIdReservation(reservation.getIdReservation());
        return reservationNotification;
    }
}
