package org.compain.library.service;

import org.compain.library.consumer.*;
import org.compain.library.model.Book;
import org.compain.library.model.Reservation;
import org.compain.library.model.User;
import org.compain.library.service.DTO.InfoReservation;
import org.compain.library.service.DTO.ReservationDTO;
import org.compain.library.service.DTO.ReservationNotification;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ReservationService {

    private final BookRepository bookRepository;
    private final CopyRepository copyRepository;
    private final ReservationRepository reservationRepository;
    private final UserRepository userRepository;
    private final BorrowingRepository borrowingRepository;
    private final MailService mailService;

    public ReservationService(BookRepository bookRepository, CopyRepository copyRepository, ReservationRepository reservationRepository, UserRepository userRepository, BorrowingRepository borrowingRepository, MailService mailService) {
        this.bookRepository = bookRepository;
        this.copyRepository = copyRepository;
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.borrowingRepository = borrowingRepository;
        this.mailService = mailService;
    }

    public Reservation createReservation(InfoReservation infoReservation, Long idUser) {
        if (infoReservation.getReservable() == true) {
            Reservation reservation = new Reservation();
            Optional<User> user = userRepository.findByIdUser(idUser);
            if (user.isPresent()) {
                Book book = bookRepository.findByIdBook(infoReservation.getIdBook());
                if (!reservationRepository.existsByUserAndBook(user.get(), book)) {
                    if (!borrowingRepository.findByBookAndUser(idUser, infoReservation.getIdBook()).isPresent()) {
                        reservation.setUser(user.get());
                        reservation.setBook(book);
                        reservation.setNotified(false);
                        reservation.setReservationDate(LocalDateTime.now());
                        reservation.setNotificationDate(null);
                    }
                }
            }
            return reservation;
        }
        return null;
    }

    public void save(Reservation reservation){
        reservationRepository.save(reservation);
    }

    public void deleteReservation(Long idReservation) {
        reservationRepository.deleteById(idReservation);
    }

    public List<ReservationDTO> getUserReservation(Long idUser) {
        List<Reservation> reservationList = reservationRepository.findReservationByUser(idUser);
        return reservationList.stream().map(ReservationMapper::toDTO).collect(Collectors.toList());
    }

    public List<ReservationDTO> getReservationsForBook(Long idBook) {
        Book book = bookRepository.findByIdBook(idBook);
        return reservationRepository.findByBook(book).stream().map(ReservationMapper::toDTO).collect(Collectors.toList());
    }

    public Reservation getFirstReservationForBook(Long idBook, Long idLibrary) {
        return reservationRepository.findFirstReservationForBook(idBook, idLibrary);
    }

    public List<ReservationNotification> findReservationToReplaceLateReservation() {
        List<Reservation> lateReservations = reservationRepository.findReservationNotified();
        List<ReservationNotification> reservationNotificationList = new ArrayList<>();
        for (Reservation reservation : lateReservations
        ) {
            if (reservation.getNotificationDate().plusDays(2).isBefore(LocalDateTime.now()) && reservation.getNotified() == true) {
                Book book = reservation.getBook();
                Long idLibrary = reservation.getUser().getLibrary().getIdLibrary();
                reservationRepository.delete(reservation);
                Reservation firstReservation = getFirstReservationForBook(book.getIdBook(), idLibrary);
                reservationNotificationList.add(ReservationMapper.toReservationNotification(firstReservation));
            }
        }
        return reservationNotificationList;
    }

    public Optional<Reservation> sendMailForNotifyReservation(ReservationNotification reservationNotification) {
        return reservationRepository.findById(reservationNotification.getIdReservation()).map(r -> {
            mailService.sendMailForNotifyReservation(reservationNotification);
            r.setNotified(true);
            r.setNotificationDate(LocalDateTime.now());
            return reservationRepository.save(r);
        });
    }

    public Integer getMyPostion(Long idUser, Long idBook) {
        Book book = bookRepository.findByIdBook(idBook);
        Optional<User> user = userRepository.findByIdUser(idUser);
        Integer position = 0;
        if (user.isPresent()) {
            if (reservationRepository.existsByUserAndBook(user.get(), book)) {

                List<Reservation> reservationList = reservationRepository.findReservationByBook(idBook);
                for (Reservation reservation : reservationList
                ) {
                    position = position + 1;
                    if (reservation.getUser().getIdUser() == idUser) {
                        break;
                    }
                }
            }
        }
        return position;
    }

    public Integer getMaxReservation(Long idBook, Long idLibrary) {
        return copyRepository.countCopiesNumberByBookAndLibrary(idBook, idLibrary) * 2;
    }

    public InfoReservation getBookReservationInfo(Long userId, Long idBook) {
        if (userRepository.findByIdUser(userId).isPresent()) {
            User user = userRepository.findByIdUser(userId).get();
            //List<Reservation> reservationList = reservationRepository.findReservationByBook(idBook);
            InfoReservation infoReservation = new InfoReservation();
            infoReservation.setIdBook(idBook);
            infoReservation.setMaxReservation(getMaxReservation(idBook, user.getLibrary().getIdLibrary()));
            infoReservation.setCurrentReservation(reservationRepository.numberOfReservationForBook(idBook));
            infoReservation.setFirstReturn(borrowingRepository.findFirstReturn(idBook));
            if (infoReservation.getMaxReservation() <= infoReservation.getCurrentReservation() || borrowingRepository.findByBookAndUser(userId, idBook).isPresent() || reservationRepository.existsByUserAndBook(user, bookRepository.findByIdBook(idBook))) {
                infoReservation.setReservable(false);
            } else {

                infoReservation.setReservable(true);
            }
            infoReservation.setMyPosition(getMyPostion(userId, idBook));
            return infoReservation;
        } else {
            return null;
        }
    }
}