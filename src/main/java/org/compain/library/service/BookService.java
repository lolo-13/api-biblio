package org.compain.library.service;
import org.compain.library.consumer.*;
import org.compain.library.model.Book;
import org.compain.library.model.User;
import org.compain.library.service.DTO.BookDTO;
import org.compain.library.service.DTO.InfoReservation;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import java.util.Optional;


import static java.util.stream.Collectors.toList;

@Service
public class BookService {

    private final BookRepository bookRepository;
    private final CopyRepository copyRepository;
    private final ReservationRepository reservationRepository;
    private final UserRepository userRepository;
    private final BorrowingRepository borrowingRepository;

    public BookService(BookRepository bookRepository, CopyRepository copyRepository, ReservationRepository reservationRepository, UserRepository userRepository, BorrowingRepository borrowingRepository) {
        this.bookRepository = bookRepository;
        this.copyRepository = copyRepository;
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.borrowingRepository = borrowingRepository;
    }


    public BookDTO findById(Long idBook, Long idLibrary){
        BookDTO bookDto = BookMapper.toDTO(bookRepository.findByIdBook(idBook));
        bookDto.setNumberOfCopies(copyRepository.countAvailableCopiesNumberByBookAndLibrary(bookDto.getIdBook(), idLibrary));
        return bookDto;
    }

    public List<BookDTO> findAll() {
        List<Book> books = bookRepository.findAll();
        return books.stream().map(BookMapper::toDTO).collect(toList());
    }

    public List<BookDTO> findAllByLibrary(Long id) {
        List<Book> books = bookRepository.findBookByLibrary(id);
        return books.stream().map(b-> Map.entry(b, copyRepository.countAvailableCopiesNumberByBookAndLibrary(b.getIdBook(),id)))
                .map(BookMapper::toDTO).collect(toList());

    }

    public List<BookDTO> findAllAvailableByLibrary(Long id) {
        List<Book> books = bookRepository.findBookByLibrary(id);
        return books.stream().map(b-> Map.entry(b, copyRepository.countAvailableCopiesNumberByBookAndLibrary(b.getIdBook(),id)))
                .map(BookMapper::toDTO).collect(toList());

    }

    public List<BookDTO> search(String title, String authorName, String categoryName, Long idLibrary) {
        List<Book> books = bookRepository.search(title, authorName, categoryName);
        return books.stream().map(b-> Map.entry(b, copyRepository.countAvailableCopiesNumberByBookAndLibrary(b.getIdBook(),idLibrary)))
                .map(BookMapper::toDTO).collect(toList());
    }

    public void deleteBook(Long idBook){
        bookRepository.deleteById(idBook);
    }

    public void createBook(Book book){
        bookRepository.save(book);
    }

    public InfoReservation getBookInfoReservation(Long idBook, Long idLibrary, Long idUser) {
        Optional<User> user = userRepository.findByIdUser(idUser);
        Book book = bookRepository.findByIdBook(idBook);
        InfoReservation infoReservation = new InfoReservation();
        infoReservation.setIdBook(idBook);
        infoReservation.setMaxReservation(copyRepository.countCopiesNumberByBookAndLibrary(idBook, idLibrary) * 2);
        infoReservation.setCurrentReservation(reservationRepository.numberOfReservationForBook(idBook));
        if (user.isPresent()) {
            if (borrowingRepository.findByBookAndUser(idUser, idBook).isPresent()) {
                infoReservation.setReservable(false);
            } else {
                infoReservation.setReservable(true);
            }
        }
        infoReservation.setFirstReturn(borrowingRepository.findFirstReturn(idBook));
        return infoReservation;
    }

}
