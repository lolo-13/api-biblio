package org.compain.library.service;

import org.compain.library.consumer.*;
import org.compain.library.model.*;
import org.compain.library.service.DTO.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.mail.MessagingException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class BorrowingServiceTest {


    @Mock
    private CopyRepository copyRepositoryMock;
    @Mock
    private UserRepository userRepositoryMock;
    @Mock
    private BorrowingRepository borrowingRepositoryMock;
    @Mock
    private ReservationService reservationServiceMock;

    BorrowingService borrowingServiceTest;

    @BeforeEach
    public void init() {
        borrowingServiceTest = new BorrowingService(borrowingRepositoryMock, userRepositoryMock, copyRepositoryMock, reservationServiceMock);
    }

    @Test
    public void renewBorrowingTest() {
        Borrowing borrowing = new Borrowing();
        borrowing.setRenewal(false);
        borrowing.setBorrowingLimitDate(LocalDateTime.of(2022, 5, 1, 12, 20));

        when(borrowingRepositoryMock.findByIdBorrowing(Mockito.any())).thenReturn(borrowing);

        Borrowing renewBorring = borrowingServiceTest.renewBorrowing(1L);

        verify(borrowingRepositoryMock, times(1)).save(eq(borrowing));
        Assertions.assertTrue(renewBorring.getRenewal());
        Assertions.assertEquals(LocalDateTime.of(2022, 5, 1, 12, 20).plusWeeks(4), renewBorring.getBorrowingLimitDate());
    }

    @Test
    public void deleteReservationForNewBorrowingTest() {
        List<ReservationDTO> reservationDTOList = new ArrayList<>();
        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setIdReservation(1L);
        reservationDTO.setIdBook(1L);
        ReservationDTO reservationDTO2 = new ReservationDTO();
        reservationDTO2.setIdReservation(2L);
        reservationDTO2.setIdBook(2L);
        reservationDTOList.add(reservationDTO);
        reservationDTOList.add(reservationDTO2);
        Copy copy = new Copy();
        Book book = new Book();
        book.setIdBook(1L);
        copy.setBook(book);

        ReservationDTO erasedReservation = borrowingServiceTest.deleteReservationForNewBorrowing(reservationDTOList, copy);

        verify(reservationServiceMock, times(1)).deleteReservation(eq(erasedReservation.getIdReservation()));
        Assertions.assertEquals(1L, erasedReservation.getIdReservation());

    }

    @Test
    public void returnBorrowingTest() throws MessagingException {
        User user = new User();
        Library library = new Library();
        user.setLibrary(library);
        Copy copy = new Copy();
        Book book = new Book();
        copy.setBook(book);
        Optional<User> optionalUser = Optional.of(user);
        Borrowing borrowing = new Borrowing();
        borrowing.setReturned(false);
        borrowing.setCopy(copy);
        Reservation reservation = new Reservation();
        reservation.setUser(user);
        reservation.setBook(book);

        when(userRepositoryMock.findByIdUser(Mockito.any())).thenReturn(optionalUser);
        when(borrowingRepositoryMock.findByIdBorrowing(Mockito.any())).thenReturn(borrowing);
        when(reservationServiceMock.getFirstReservationForBook(Mockito.any(), Mockito.any())).thenReturn(reservation);

        Borrowing returnedBorrowing = borrowingServiceTest.returnBorrowing(1L, 1L);

        Assertions.assertTrue(returnedBorrowing.getReturned());
        Assertions.assertTrue(returnedBorrowing.getCopy().getAvailable());
        verify(reservationServiceMock, times(1)).sendMailForNotifyReservation(eq(ReservationMapper.toReservationNotification(reservation)));
    }

    @Test
    public void createNewBorrowingTest() {
        BorrowingDTO borrowingDTO = new BorrowingDTO();
        UserDTO userDTO = new UserDTO();
        LibraryDTO libraryDTO = new LibraryDTO();
        CopyDTO copyDTO = new CopyDTO();
        BookDTO bookDTO = new BookDTO();
        copyDTO.setBookDto(bookDTO);
        copyDTO.setLibraryDto(libraryDTO);
        copyDTO.setIdCopy(1L);
        borrowingDTO.setCopyDto(copyDTO);
        borrowingDTO.setUserDto(userDTO);
        borrowingDTO.setReturned(false);
        borrowingDTO.setRenewal(true);

        User user = new User();
        user.setIdUser(1L);
        Optional<User> optionalUser = Optional.of(user);
        Library library = new Library();
        user.setLibrary(library);
        Copy copy = new Copy();
        copy.setAvailable(true);
        Optional<Copy> optionalCopy = Optional.of(copy);
        Book book = new Book();
        book.setIdBook(1L);
        copy.setBook(book);

        List<ReservationDTO> reservationDTOList = new ArrayList<>();
        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setIdReservation(1L);
        reservationDTO.setIdBook(1L);
        ReservationDTO reservationDTO2 = new ReservationDTO();
        reservationDTO2.setIdReservation(2L);
        reservationDTO2.setIdBook(2L);
        reservationDTOList.add(reservationDTO);
        reservationDTOList.add(reservationDTO2);

        when(userRepositoryMock.findByUsername(Mockito.any())).thenReturn(optionalUser);
        when(copyRepositoryMock.findById(Mockito.any())).thenReturn(optionalCopy);
        when(reservationServiceMock.getUserReservation(Mockito.any())).thenReturn(reservationDTOList);

        Borrowing newBorrowing = borrowingServiceTest.createNewBorrowing(borrowingDTO);

        Assertions.assertTrue(newBorrowing.getRenewal());
        Assertions.assertFalse(newBorrowing.getReturned());
        Assertions.assertEquals(1L, newBorrowing.getUser().getIdUser());

    }

}
