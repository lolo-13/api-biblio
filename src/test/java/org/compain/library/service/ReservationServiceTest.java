package org.compain.library.service;

import org.compain.library.consumer.*;
import org.compain.library.model.*;
import org.compain.library.service.DTO.InfoReservation;
import org.compain.library.service.DTO.ReservationDTO;
import org.compain.library.service.DTO.ReservationNotification;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class ReservationServiceTest {

    @Mock
    private BookRepository bookRepositoryMock;
    @Mock
    private CopyRepository copyRepositoryMock;
    @Mock
    private ReservationRepository reservationRepositoryMock;
    @Mock
    private UserRepository userRepositoryMock;
    @Mock
    private BorrowingRepository borrowingRepositoryMock;
    @Mock
    private MailService mailServiceMock;

    ReservationService reservationServiceTest;

    @BeforeEach
    public void init() {
        reservationServiceTest = new ReservationService(bookRepositoryMock, copyRepositoryMock, reservationRepositoryMock, userRepositoryMock, borrowingRepositoryMock, mailServiceMock);
    }

    @Test
    public void getReservationsForBookTest() throws NullPointerException {

        Book book = new Book();
        book.setIdBook(1L);
        User user = new User();
        user.setIdUser(1L);
        Reservation reservation = new Reservation();
        reservation.setBook(book);
        reservation.setIdReservation(1L);
        reservation.setUser(user);
        reservation.setReservationDate(LocalDateTime.now());
        Reservation reservation2 = new Reservation();
        reservation2.setIdReservation(2L);
        reservation2.setUser(user);
        reservation2.setBook(book);
        reservation2.setReservationDate(LocalDateTime.now());


        List<Reservation> reservationList = new ArrayList<>();
        reservationList.add(reservation);
        reservationList.add(reservation2);
        when(bookRepositoryMock.findByIdBook(Mockito.any())).thenReturn(book);
        when(reservationRepositoryMock.findByBook(book)).thenReturn(reservationList);

        List<ReservationDTO> reservationDTOList = reservationServiceTest.getReservationsForBook(1L);
        Assertions.assertEquals(2, reservationDTOList.size());

    }

    @Test
    public void getMyPositionTest() {
        Book book = new Book();
        book.setIdBook(1L);
        User user = new User();
        user.setIdUser(1L);
        User user2 = new User();
        user2.setIdUser(2L);
        Reservation reservation = new Reservation();
        reservation.setBook(book);
        reservation.setIdReservation(1L);
        reservation.setUser(user);
        reservation.setReservationDate(LocalDateTime.now());
        Reservation reservation2 = new Reservation();
        reservation2.setIdReservation(2L);
        reservation2.setUser(user2);
        reservation2.setBook(book);
        reservation2.setReservationDate(LocalDateTime.now().minusDays(3));
        List<Reservation> reservationList = new ArrayList<>();
        reservationList.add(reservation2);
        reservationList.add(reservation);

        when(bookRepositoryMock.findByIdBook(Mockito.any())).thenReturn(book);
        when(userRepositoryMock.findByIdUser(Mockito.any())).thenReturn(java.util.Optional.of(user));
        when(reservationRepositoryMock.existsByUserAndBook(user, book)).thenReturn(true);
        when(reservationRepositoryMock.findReservationByBook(book.getIdBook())).thenReturn(reservationList);

        Integer userPosition = reservationServiceTest.getMyPostion(1L, 1L);
        Integer user2Position = reservationServiceTest.getMyPostion(2L, 1L);

        Assertions.assertTrue(user2Position < userPosition);
        Assertions.assertEquals(2, userPosition);

    }

    @Test
    public void getBookReservationInfoTest() {
        Library library = new Library();
        library.setIdLibrary(1L);
        Book book = new Book();
        book.setIdBook(1L);
        User user = new User();
        user.setIdUser(1L);
        user.setLibrary(library);
        Reservation reservation = new Reservation();
        reservation.setBook(book);
        reservation.setIdReservation(1L);
        reservation.setUser(user);
        reservation.setReservationDate(LocalDateTime.now());
        Reservation reservation2 = new Reservation();

        when(bookRepositoryMock.findByIdBook(1L)).thenReturn(book);
        when(userRepositoryMock.findByIdUser(1L)).thenReturn(java.util.Optional.of(user));
        when(copyRepositoryMock.countCopiesNumberByBookAndLibrary(Mockito.any(), Mockito.any())).thenReturn(6);
        when(reservationRepositoryMock.numberOfReservationForBook(Mockito.any())).thenReturn(2);
        when(borrowingRepositoryMock.findFirstReturn(Mockito.any())).thenReturn(LocalDateTime.now().plusDays(7));

        InfoReservation infoReservation = reservationServiceTest.getBookReservationInfo(1L, 1L);

        Assertions.assertEquals(12, infoReservation.getMaxReservation());
        Assertions.assertEquals(2, infoReservation.getCurrentReservation());
        Assertions.assertTrue(infoReservation.getReservable());

    }

    @Test
    public void findReservationToReplaceLateReservationTest() {
        Library library = new Library();
        library.setIdLibrary(1L);
        Book book = new Book();
        book.setIdBook(1L);
        User user = new User();
        user.setIdUser(1L);
        user.setLibrary(library);
        Reservation reservation = new Reservation();
        reservation.setBook(book);
        reservation.setIdReservation(1L);
        reservation.setUser(user);
        reservation.setReservationDate(LocalDateTime.now().minusDays(45));
        reservation.setNotified(true);
        reservation.setNotificationDate(LocalDateTime.now().minusDays(4));
        Reservation reservation2 = new Reservation();
        reservation2.setIdReservation(2L);
        reservation2.setUser(user);
        reservation2.setBook(book);
        reservation2.setReservationDate(LocalDateTime.now().minusDays(20));
        reservation2.setNotified(true);
        reservation2.setNotificationDate(LocalDateTime.now().minusDays(1));
        List<Reservation> reservationList = new ArrayList<>();
        reservationList.add(reservation);
        reservationList.add(reservation2);


        when(reservationRepositoryMock.findReservationNotified()).thenReturn(reservationList);
        when(reservationRepositoryMock.findFirstReservationForBook(Mockito.any(), Mockito.any())).thenReturn(reservation);

        List<ReservationNotification> reservationNotificationList = reservationServiceTest.findReservationToReplaceLateReservation();

        Assertions.assertEquals(1, reservationNotificationList.size());
        Assertions.assertEquals(1L, reservationNotificationList.get(0).getIdReservation());

    }

    @Test
    public void sendMailForNotifyReservationTest() {
        Library library = new Library();
        library.setIdLibrary(1L);
        Book book = new Book();
        book.setIdBook(1L);
        User user = new User();
        user.setIdUser(1L);
        user.setLibrary(library);
        Reservation reservation = new Reservation();
        reservation.setBook(book);
        reservation.setIdReservation(1L);
        reservation.setUser(user);
        reservation.setReservationDate(LocalDateTime.now());
        ReservationNotification reservationNotification = new ReservationNotification();

        when(reservationRepositoryMock.findById(Mockito.any())).thenReturn(java.util.Optional.of(reservation));
        when(reservationRepositoryMock.save(Mockito.any())).thenReturn(reservation.setNotified(true));

        Optional<Reservation> reservation1 = reservationServiceTest.sendMailForNotifyReservation(reservationNotification);

        Assertions.assertEquals(true, reservation1.get().getNotified());

    }

    @Test
    public void getMaxReservationTest() {
        when(copyRepositoryMock.countCopiesNumberByBookAndLibrary(Mockito.any(), Mockito.any())).thenReturn(2);
        Integer maxReservation = reservationServiceTest.getMaxReservation(1L, 1L);
        Assertions.assertEquals(4, maxReservation);
    }

    @Test
    public void deleteReservationTest() {
        Long idReservation = 42L;

        reservationServiceTest.deleteReservation(idReservation);

        verify(reservationRepositoryMock, times(1)).deleteById(eq(idReservation));
    }

    @Test
    public void createReservationTest() {
        InfoReservation infoReservation = new InfoReservation();
        infoReservation.setReservable(true);
        infoReservation.setIdBook(1L);
        Library library = new Library();
        library.setIdLibrary(1L);
        User user = new User();
        user.setIdUser(1L);
        user.setLibrary(library);
        Book book = new Book();
        book.setIdBook(1L);


        when(userRepositoryMock.findByIdUser(Mockito.any())).thenReturn(Optional.of(user));
        when(bookRepositoryMock.findByIdBook(Mockito.any())).thenReturn(book);
        when(reservationRepositoryMock.existsByUserAndBook(Mockito.any(), Mockito.any())).thenReturn(false);
        when(borrowingRepositoryMock.findByBookAndUser(Mockito.any(), Mockito.any())).thenReturn(Optional.<Borrowing>empty());

        Reservation reservation = reservationServiceTest.createReservation(infoReservation, 1L);

        Assertions.assertFalse(reservation.getNotified());
        Assertions.assertEquals(1L, reservation.getUser().getIdUser());

    }
}
